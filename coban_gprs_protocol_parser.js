module.exports.pharse = function(data) {
  //console.log(data)
    var result =null;
    if (data.indexOf("##") === 0){
      result = login(data)
    }else if (data.indexOf(",") > -1) {
      result = messege(data)
    }else{
      result = heartbeat(data)
    }
    if(result == null){
      console.log("Error pharsing messege");
    }else {
      return result
    }
}
    function login(data){
      var re = /##,imei:(\d*).*;/

      var result = data.match(re)
      if(result!=null){
        if(result[1] != ""){
          return {'imei':result[1],'type': 'login', 'response': 'LOAD'}
        }
      }
    return null
    }
    function heartbeat(data){
      var re = /(\d*)/

      var result = data.match(re)
      if(result!=null){
        if(result[1] != ""){
          return {'imei':result[1],'type': 'heartbeat', 'response': 'ON'}
        }
      }
    return null
    }
    function messege(data){
      splited_content = data.replace('imei:','').replace(';','').split(',', -1)
      switch (splited_content[1]) {
        case '001':
                message_headers = [
                  'imei', 'type', 'date', 'phone', 'gps_status', 'time', 'gps_signal', 'latitude', 'north_south', 'longitude',
                  'east_west', 'speed', 'direction', 'elevation', 'acc', 'door', 'fuel', 'oil', 'temperatura'
                ]
                result = {
                  'imei': splited_content[message_headers.indexOf('imei')],
                  'type': splited_content[message_headers.indexOf('type')],
                  'phone': splited_content[message_headers.indexOf('phone')],
                  'gps_status': splited_content[message_headers.indexOf('gps_status')],
                  'gps_signal': splited_content[message_headers.indexOf('gps_signal')],
                  'speed': splited_content[message_headers.indexOf('speed')],
                  'direction': splited_content[message_headers.indexOf('direction')]
                }
                if (splited_content.length > 13){
                  result['elevation'] = splited_content[message_headers.indexOf('elevation')]
                  result['acc'] = splited_content[message_headers.indexOf('acc')]
                  result['door'] = splited_content[message_headers.indexOf('door')]
                  result['fuel'] = splited_content[message_headers.indexOf('fuel')]
                  result['oil'] = splited_content[message_headers.indexOf('oil')]
                  result['temperatura'] = splited_content[message_headers.indexOf('temperatura')]
                }
                pre_latitude = splited_content[message_headers.indexOf('latitude')]
                latitude = parseInt(pre_latitude.substring(0,2),10) + ( parseFloat(pre_latitude.substring(2,pre_latitude.length)) / 60 )
                if(splited_content[message_headers.indexOf("north_south")] == 'S'){
                  latitude *= -1
                }
                pre_longitude = splited_content[message_headers.indexOf('longitude')]
                longitude = parseInt(pre_longitude.substring(0,3),10) + ( parseFloat(pre_longitude.substring(3,pre_longitude.length)) / 60 )
                if(splited_content[message_headers.indexOf('east_west')] == 'W'){
                  longitude *= -1
                }

                result['latitude'] = latitude
                result['longitude'] = longitude

                // date = splited_content[message_headers.indexOf('date')]
                // time = splited_content[message_headers.indexOf('time')]
                // unless date.size == date.count('0')
                //   time = splited_content[message_headers.index(:time)]
                //   result[:date] = DateTime.strptime('20' + date[0...6] + time.split('.').first,'%Y%m%d%H%M%S')
                // end
                date = splited_content[message_headers.indexOf('date')]
                time = splited_content[message_headers.indexOf('time')]
                result['date'] = "20" + date.substring(0,2)+"-"+date.substring(2,4)+"-"+date.substring(4,6)+' '+date.substring(6,8)+':'+date.substring(8,10)+':'+date.substring(10,12)

                result['sub_type'] = result['type']
                result['type'] = 'message'
                result['response'] = null


          break;
        case 'OBD':
                message_headers = [
                  'imei', 'type', 'date', 'mileage', 'instant_fuel', 'average_fuel', 'driving_time', 'speed', 'power_load', 'water_temperature',
                  'engine_speed', 'battery_voltage', 'dtc1', 'dtc2', 'dtc3', 'dtc4'
                ]
                result = {
                  'imei': splited_content[message_headers.indexOf('imei')],
                  //'type': splited_content[message_headers.indexOf('type')],
                  'date': splited_content[message_headers.indexOf('date')],
                  'mileage': splited_content[message_headers.indexOf('mileage')],
                  'instant_fuel': splited_content[message_headers.indexOf('instant_fuel')],
                  'average_fuel': splited_content[message_headers.indexOf('average_fuel')],
                  'driving_time': splited_content[message_headers.indexOf('driving_time')],
                  'speed': splited_content[message_headers.indexOf('speed')],
                  'power_load': splited_content[message_headers.indexOf('power_load')],
                  'water_temperature': splited_content[message_headers.indexOf('water_temperature')],
                  'engine_speed': splited_content[message_headers.indexOf('engine_speed')],
                  'battery_voltage': splited_content[message_headers.indexOf('battery_voltage')],
                  'dtc1': splited_content[message_headers.indexOf('dtc1')],
                  'dtc2': splited_content[message_headers.indexOf('dtc2')],
                  'dtc3': splited_content[message_headers.indexOf('dtc3')],
                  'dtc4': splited_content[message_headers.indexOf('dtc4')]
                }

                // date = splited_content[message_headers.indexOf('date')]
                // time = splited_content[message_headers.indexOf('time')]
                // unless date.size == date.count('0')
                //   time = splited_content[message_headers.index(:time)]
                //   result[:date] = DateTime.strptime('20' + date[0...6] + time.split('.').first,'%Y%m%d%H%M%S')
                // end
                date = splited_content[message_headers.indexOf('date')]
                result['date'] = "20" + date.substring(0,2)+"-"+date.substring(2,4)+"-"+date.substring(4,6)+' '+date.substring(6,8)+':'+date.substring(8,10)+':'+date.substring(10,12)

                result['sub_type'] = result['type']
                result['type'] = 'message'
                result['response'] = null


          break;
        default:
        }


      return result
    }

// Open a connection automatically at app startup.
//module.exports.pharse;
